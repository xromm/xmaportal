## The [X-ray Motion Analysis (XMA) Portal](http://xmaportal.org) is a web based data management system specifically developed for [XROMM](http://www.xromm.org) data.

## Visit the [XMAPortal Wiki](https://wiki.brown.edu/confluence/display/ctx/XMAPortal+User+Manual) for getting started and user manual.
